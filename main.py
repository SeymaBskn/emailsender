import smtplib
from email.message import EmailMessage
from string import Template
from pathlib import Path

html = Template(Path("index.html").read_text())

email = EmailMessage()
email["from"] = "Sender info"
email["to"] = "xx@gmail.com"
email["subject"] = "Subject of the email"

email.set_content(html.substitute({"name":"Tintin"}))

with smtplib.SMTP(host="smtp.gmail.com", port=587) as smtp:
    smtp.ehlo()
    smtp.starttls()
    smtp.login(user, password)
    smtp.send_message(email)
    print("Email sent")
